﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LightMage.Util
{
    class ConcurrentFixedSizeQueue<T> : IProducerConsumerCollection<T>, IReadOnlyCollection<T>, ICollection
    {
        readonly ConcurrentQueue<T> _mConcurrentQueue;
        readonly int _mMaxSize;

        public int Count => _mConcurrentQueue.Count;
        public bool IsEmpty => _mConcurrentQueue.IsEmpty;

        public ConcurrentFixedSizeQueue(int maxSize) : this(Array.Empty<T>(), maxSize)
        {
        }

        public ConcurrentFixedSizeQueue(IEnumerable<T> initialCollection, int maxSize)
        {
            if (initialCollection == null)
            {
                throw new ArgumentNullException(nameof(initialCollection));
            }

            _mConcurrentQueue = new ConcurrentQueue<T>(initialCollection);
            _mMaxSize = maxSize;
        }

        public void Enqueue(T item)
        {
            _mConcurrentQueue.Enqueue(item);

            if (_mConcurrentQueue.Count > _mMaxSize)
            {
                _mConcurrentQueue.TryDequeue(out _);
            }
        }

        public void TryPeek(out T result) => _mConcurrentQueue.TryPeek(out result);
        public bool TryDequeue(out T result) => _mConcurrentQueue.TryDequeue(out result);

        public void CopyTo(T[] array, int index) => _mConcurrentQueue.CopyTo(array, index);
        public T[] ToArray() => _mConcurrentQueue.ToArray();

        public IEnumerator<T> GetEnumerator() => _mConcurrentQueue.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        // Explicit ICollection implementations.
        void ICollection.CopyTo(Array array, int index) => ((ICollection) _mConcurrentQueue).CopyTo(array, index);
        object ICollection.SyncRoot => ((ICollection) _mConcurrentQueue).SyncRoot;
        bool ICollection.IsSynchronized => ((ICollection) _mConcurrentQueue).IsSynchronized;

        // Explicit IProducerConsumerCollection<T> implementations.
        bool IProducerConsumerCollection<T>.TryAdd(T item) =>
            ((IProducerConsumerCollection<T>) _mConcurrentQueue).TryAdd(item);

        bool IProducerConsumerCollection<T>.TryTake(out T item) =>
            ((IProducerConsumerCollection<T>) _mConcurrentQueue).TryTake(out item);

        public override int GetHashCode() => _mConcurrentQueue.GetHashCode();
        public override bool Equals(object obj) => _mConcurrentQueue.Equals(obj);
        public override string ToString() => _mConcurrentQueue.ToString();
    }
}