﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace LightMage.Curve
{
    public class LinearStrategy : IStrategy
    {
        public string Name => "Linear";

        public byte GetBrightness(ISet<DataPoint> points, float illuminance)
        {
            var ordered = points
                .OrderBy(v => v.Illuminance)
                .ToList();

            var (firstP, secondP) = ordered
                .Pairwise((first, second) => (first, second))
                .FirstOrDefault(pair => illuminance >= pair.first.Illuminance && illuminance < pair.second.Illuminance);

            if (firstP == null || secondP == null)
            {
                return illuminance < ordered.First().Illuminance
                    ? ordered.First().Brightness
                    : ordered.Last().Brightness;
            }

            return Calc(firstP.Brightness, secondP.Brightness, firstP.Illuminance, secondP.Illuminance, illuminance);
        }

        private static byte Calc(byte b1, byte b2, float i1, float i2, float i) =>
            (byte) Math.Round((i - i1) / (i2 - i1) * (b2 - b1) + b1);
    }
}