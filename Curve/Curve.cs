﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightMage.Annotations;

namespace LightMage.Curve
{
    public class Curve
    {
        [NotNull] public IStrategy Strategy { get; set; }
        [NotNull] public ISet<DataPoint> Points { get; set; }

        public Curve(IStrategy strategy, ISet<DataPoint> points)
        {
            Strategy = strategy;
            Points = points;
        }

        public byte CalculateBrightness(float illuminance)
        {
            return Strategy.GetBrightness(Points, illuminance);
        }
    }
}
