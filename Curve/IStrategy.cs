﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LightMage.Curve
{
    public interface IStrategy
    {
        string Name { get; }
        byte GetBrightness(ISet<DataPoint> points, float illuminance);
    }
}
