﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightMage.Curve
{
    public class DataPoint
    {
        public DataPoint(float illuminance, byte brightness)
        {
            Illuminance = illuminance;
            Brightness = brightness;
        }

        public float Illuminance { get; }
        public byte Brightness { get; }

        public override bool Equals(object o)
        {
            if (o is DataPoint oPoint)
            {
                return Brightness == oPoint.Brightness || Illuminance == oPoint.Illuminance;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (Illuminance, Brightness).GetHashCode();
        }

        public static bool operator ==(DataPoint p1, DataPoint p2)
        {
            if (p1 is null)
                return p2 is null;
            if (p2 is null)
                return false;
            return p1.Equals(p2);
        }

        public static bool operator !=(DataPoint p1, DataPoint p2)
        {
            return !(p1 == p2);
        }
    }
}
