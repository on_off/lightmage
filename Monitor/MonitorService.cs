﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightMage.Monitor
{
    public interface MonitorService : IDisposable
    {
        uint brightness { get; set; }
    }
}