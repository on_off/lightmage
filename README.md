# LightMage
Autobrightness utility to emulate typical smartphone behaviour.
Obtains ambient luminocity through Windows Sensor API, therefore requiers HID-compatible sensor
[(wink wink, nudge nudge)](https://gitlab.com/on_off/arduino-hid-lightsensor)

VERY unfinished, luminocity -> brightness values are hardcoded, and all the monitors' brightness is synced and code quality suffers, but i will probably improve it later.

Brightness adjustment uses DDC/CI and is based on [Brighty by Aldaviva](https://github.com/Aldaviva/Brighty).