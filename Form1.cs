﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Windows.Devices.Sensors;
using LightMage.Util;
using LightMage.Curve;
using LightMage.Monitor;

namespace LightMage
{
    public partial class Form1 : Form
    {
        private readonly ConcurrentFixedSizeQueue<float> _measures = new ConcurrentFixedSizeQueue<float>(30);
        private readonly LightSensor _sensor = LightSensor.GetDefault();
        private float _lastAverage = 0;

        private readonly Curve.Curve _curve = new Curve.Curve(new LinearStrategy(), new HashSet<DataPoint>
        {
            new DataPoint(0f, 0),
            new DataPoint(80f, 10),
            new DataPoint(200f, 23),
            new DataPoint(10000f, 100),
        });

        private readonly MonitorService _monitorService = new DirectXVideoAccelerationMonitorService();

        public Form1()
        {
            InitializeComponent();
            // infoBindingSource.DataSource = new Data();
            // sensor.ReadingChanged += (sender, args) => { label1.Text = $"{sender.GetCurrentReading().IlluminanceInLux}"; };
            timer.Tick += UpdateMeasures;
        }

        private void UpdateMeasures(object sender, EventArgs e)
        {
            _measures.Enqueue(_sensor.GetCurrentReading().IlluminanceInLux);
            var average = _measures.Average();
            var brightness = _curve.CalculateBrightness(average);
            if (brightness != _monitorService.brightness || Math.Abs(_lastAverage - average) > 0.1)
            {
                trayStrip.Items["percentageItem"].Text = $"{brightness}% @ {average}lux";
            }

            _lastAverage = average;
            if (brightness != _monitorService.brightness)
            {
                _monitorService.brightness = brightness;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }

        private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Hide();
        }
    }
}